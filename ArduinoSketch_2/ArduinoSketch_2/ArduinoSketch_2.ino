/*
 Name:		ArduinoSketch_2.ino
 Created:	2/4/2020 8:56:01 PM
 Author:	Eye-F
*/

// the setup function runs once when you press reset or power the board
#include "LED.h"
#include "Pin.h"
#include "Button.h"
void setup() {
	LED_Init();
	Pin_init(13);
}

// the loop function runs over and over again until power down or reset
void loop() {
  
}
