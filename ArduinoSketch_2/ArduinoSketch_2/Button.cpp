#include "Button.h"
#include "Arduino.h"
#include "LED.h"

/*Initialize the Button pin.*/
const int ButtonPin = 2;
/* Variables for reading the pushbutton status */
static int ButtonPressed;
int ButtonState = 0;

/*Initializes the button. */
void Button_Init(void)
{
	/* Initializing button pressed counter. */
	ButtonPressed = 0;
	/* Set the button*/
	pinMode(ButtonPin, INPUT);

}
/*Runs the loop to toggle the LED depending on the Button state.*/
void Button_Run(void)
{
	/* Read the state of the pushbutton value: */
	ButtonState = digitalRead(ButtonPin);
	/* Check if the pushbutton is pressed. If it is, the buttonState is HIGH: */
	if (ButtonState == HIGH) {
		// turn LED on:
		digitalWrite(LEDPin, HIGH);
		/* Incrementing the ButtonPressed counter once the button is pressed. */
		ButtonPressed++;
	}
	else {
		/* turn LED off. */
		digitalWrite(LEDPin, LOW);
	}

}
