#ifdef BUTTON_H
#define	BUTTON_H


/* Functions for the button */
void Button_Init(void);
void Button_Run(void);
#endif // BUTTON_H
