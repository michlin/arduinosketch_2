#include "LED.h"
#include "Arduino.h"

/*Initializes the LED.*/
void LED_Init(void)
{
	/* Set the LEDPin as output */
	pinMode(LEDPin, OUTPUT);
}

