// LED.h

#ifndef _LED_h
#define _LED_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
#define LED_H
/* Init Button and LED pins. */
const int LEDPin = 13;
/* Functions */
void LED_Init(void);
#endif

